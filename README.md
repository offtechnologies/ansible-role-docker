ansible-role-docker
=========

[![pipeline status](https://gitlab.com/offtechnologies/ansible-role-docker/badges/master/pipeline.svg)](https://gitlab.com/offtechnologies/ansible-role-docker/commits/master)

[offtechurl]: https://gitlab.com/offtechnologies
[![offtechnologies](https://gitlab.com/offtechnologies/logos/raw/master/logo100.png)][offtechurl]

Install / Configure Docker and Docker Compose using Ansible.

Requirements
------------

None

Role Variables
--------------

See defaults/main.yml

Dependencies
------------

None

Example Playbook
----------------

```yaml
---

- name: Configure app server(s)
  hosts: servers
  become: True
  roles:
    - { role: ansible-role-docker}
```

Supported Platforms
--------------

Debian 9 (Stretch)

License
-------

BSD

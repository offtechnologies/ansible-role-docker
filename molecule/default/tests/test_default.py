import os
import pytest
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


@pytest.mark.parametrize('pkg', [
  'apt-transport-https',
  'ca-certificates',
  'software-properties-common',
  'gnupg2',
  'python-pip',
  'docker-ce'
])
def test_pkg(host, pkg):
    package = host.package(pkg)
    assert package.is_installed


def test_docker_is_enabled(host):
    docker = host.service("docker")
    assert docker.is_enabled
